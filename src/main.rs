use actix_web::{get, web, App, HttpServer, HttpResponse, Responder};

// 记录启动时间
struct AppState {
    start_time: chrono::DateTime<chrono::Utc>,
}

#[get("/")]
async fn index() -> impl Responder {
    let html = r#"
        <html>
            <head>
                <title>TODO List API</title>
            </head>
            <body>
                <h1>Welcome to Puyang's TODO List API!</h1>
                <button onclick="location.href='/todos'" style="margin-right:10px;">TODO List</button>
                <button onclick="location.href='/uptime'">Server Uptime</button>
            </body>
        </html>
    "#;
    HttpResponse::Ok().content_type("text/html").body(html)
}


#[get("/todos")]
async fn todos() -> impl Responder {
    let todos = vec![
        "Complete Actix tutorial",
        "Read Rust book chapter 4",
        "Implement a simple web server",
        "Explore async programming in Rust",
        "Review PRs from the team",
        "Prepare Actix web presentation",
    ];

    let todos_list = todos.join("\n");
    format!("TODO List:\n{}", todos_list)
}

#[get("/uptime")]
async fn uptime(data: web::Data<AppState>) -> impl Responder {
    let now = chrono::Utc::now();
    let uptime = now.signed_duration_since(data.start_time);
    format!("Server uptime: {} seconds", uptime.num_seconds())
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let start_time = chrono::Utc::now();
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(AppState { start_time }))
            .service(index)
            .service(todos)
            .service(uptime)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
